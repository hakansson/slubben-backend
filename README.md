# Slubben Backend
The backend part of _Slubben_. The frontend can be found at: https://gitlab.com/hakansson/slubben-frontend

## Setup
In order for this to be meaningful at all, you need to get API keys from SL's Trafiklabb: https://www.trafiklab.se/api

You are going to need API keys for both `SL Platsuppslag` (store in the env variable `SL_KEY_LOOKUP`) and `SL Realtidsinformation 4` (store in the env variable `SL_KEY_REALTIME`).