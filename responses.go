package main

// Lookup API:
type lookupResponse struct {
	StatusCode    int                  `json:"StatusCode"`
	Message       interface{}          `json:"Message"`
	ExecutionTime int                  `json:"ExecutionTime"`
	ResponseData  []lookupResponseData `json:"ResponseData"`
}
type lookupResponseData struct {
	Name     string      `json:"Name"`
	SiteID   string      `json:"SiteId"`
	Type     string      `json:"Type"`
	X        string      `json:"X"`
	Y        string      `json:"Y"`
	Products interface{} `json:"Products"`
}

// Realtime API:
type realtimeResponse struct {
	StatusCode    int                  `json:"StatusCode"`
	Message       interface{}          `json:"Message"`
	ExecutionTime int                  `json:"ExecutionTime"`
	ResponseData  realtimeResponseData `json:"ResponseData"`
}
type realtimeLine struct {
	GroupOfLine          string              `json:"GroupOfLine"`
	DisplayTime          string              `json:"DisplayTime"`
	TransportMode        string              `json:"TransportMode"`
	LineNumber           string              `json:"LineNumber"`
	Destination          string              `json:"Destination"`
	JourneyDirection     int                 `json:"JourneyDirection"`
	StopAreaName         string              `json:"StopAreaName"`
	StopAreaNumber       int                 `json:"StopAreaNumber"`
	StopPointNumber      int                 `json:"StopPointNumber"`
	StopPointDesignation string              `json:"StopPointDesignation"`
	TimeTabledDateTime   string              `json:"TimeTabledDateTime"`
	ExpectedDateTime     string              `json:"ExpectedDateTime"`
	JourneyNumber        int                 `json:"JourneyNumber"`
	Deviations           []realtimeDeviation `json:"Deviations"`
}
type realtimeStopInfo struct {
	StopAreaNumber int    `json:"StopAreaNumber"`
	StopAreaName   string `json:"StopAreaName"`
	TransportMode  string `json:"TransportMode"`
	GroupOfLine    string `json:"GroupOfLine"`
}
type realtimeDeviation struct {
	Text            string `json:"Text"`
	Consequence     string `json:"Consequence"`
	ImportanceLevel int    `json:"ImportanceLevel"`
}
type realtimeStopPointDeviations struct {
	StopInfo  realtimeStopInfo  `json:"StopInfo"`
	Deviation realtimeDeviation `json:"Deviation"`
}
type realtimeResponseData struct {
	LatestUpdate        string                        `json:"LatestUpdate"`
	DataAge             int                           `json:"DataAge"`
	Metros              []realtimeLine                `json:"Metros"`
	Buses               []realtimeLine                `json:"Buses"`
	Trains              []realtimeLine                `json:"Trains"`
	Trams               []realtimeLine                `json:"Trams"`
	Ships               []realtimeLine                `json:"Ships"`
	StopPointDeviations []realtimeStopPointDeviations `json:"StopPointDeviations"`
}
