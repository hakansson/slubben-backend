package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var logger echo.Logger
var slTimeLocation *time.Location

var (
	slLookupKey   = ""
	slRealtimeKey = ""

	portToUse = "8080"
)

const slTimeFormat = "2006-01-02T15:04:05"

const (
	typeMetroRed   = "METRO_RED"
	typeMetroGreen = "METRO_GREEN"
	typeMetroBlue  = "METRO_BLUE"
	typeBus        = "BUS"
	typeTrain      = "TRAIN"
	typeTram       = "TRAM"
	typeShip       = "SHIP"
	typeUnk        = "UNKNOWN"
)

type site struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	DisplayName string `json:"displayName"`
}

type siteDepartures struct {
	Lines          []siteLine  `json:"lines"`
	SiteDeviations []deviation `json:"siteDeviations"`
}

type siteLine struct {
	Type        string      `json:"type"`
	Number      string      `json:"number"`
	Destination string      `json:"destination"`
	Direction   int         `json:"direction"`
	Departures  []departure `json:"departures"`
}

type departure struct {
	Planned     time.Time   `json:"planned"`
	Expected    time.Time   `json:"expected"`
	HasExpected bool        `json:"hasExpected"`
	Deviations  []deviation `json:"deviations"`
}

type deviation struct {
	Text        string `json:"text"`
	Importance  int    `json:"importance"`
	Consequence string `json:"consequence"`
}

func searchSite(c echo.Context) error {
	type req struct {
		Query string `json:"query" query:"query"`
	}

	r := req{}
	c.Bind(&r)

	query := strings.TrimSpace(r.Query)
	if query == "" {
		return c.String(http.StatusBadRequest, "empty query")
	}

	url := fmt.Sprintf(`https://api.sl.se/api2/typeahead.json?key=%s&searchstring=%s`, slLookupKey, query)
	resp, err := http.Get(url)
	if err != nil {
		c.Logger().Warnf("failed request to SL lookup: %w", err)
		return err
	}

	if resp.StatusCode != 200 {
		c.Logger().Errorf("got error from SL")
		return c.String(http.StatusInternalServerError, "failed to get sites from SL")
	}

	lookupResp := lookupResponse{}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&lookupResp); err != nil {
		c.Logger().Errorf("failed to decode SL response: %w", err)
		return err
	}

	result := []site{}
	for _, responseSite := range lookupResp.ResponseData {
		if responseSite.Type != "Station" {
			continue
		}
		siteID, err := strconv.Atoi(responseSite.SiteID)
		if err != nil {
			c.Logger().Errorf("Failed to convert site ID %s: %w", responseSite.SiteID, err)
			continue
		}
		s := site{
			ID:          siteID,
			Name:        responseSite.Name,
			DisplayName: toDisplayName(responseSite.Name),
		}
		result = append(result, s)
	}
	return c.JSON(http.StatusOK, result)
}

func getDeparturesForSite(c echo.Context) error {
	siteID := c.Param("id")
	if _, err := strconv.Atoi(siteID); err != nil {
		return c.String(http.StatusBadRequest, "site ID must be a number")
	}
	url := fmt.Sprintf(`https://api.sl.se/api2/realtimedeparturesV4.json?key=%s&siteid=%s&TimeWindow=60`, slRealtimeKey, siteID)
	resp, err := http.Get(url)
	if err != nil {
		c.Logger().Warnf("failed request to SL realtime: %w", err)
		return err
	}

	if resp.StatusCode != 200 {
		c.Logger().Errorf("got error from SL")
		return c.String(http.StatusInternalServerError, "failed to get departures from SL")
	}

	realtimeResp := realtimeResponse{}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&realtimeResp); err != nil {
		c.Logger().Errorf("failed to decode SL response: %w", err)
		return err
	}

	result := siteDepartures{
		Lines:          []siteLine{},
		SiteDeviations: []deviation{},
	}
	for _, stopDeviation := range realtimeResp.ResponseData.StopPointDeviations {
		result.SiteDeviations = append(result.SiteDeviations, deviation{
			Text:        stopDeviation.Deviation.Text,
			Consequence: stopDeviation.Deviation.Consequence,
			Importance:  stopDeviation.Deviation.ImportanceLevel,
		})
	}

	result.Lines = append(result.Lines, parseLines(realtimeResp.ResponseData.Metros)...)
	result.Lines = append(result.Lines, parseLines(realtimeResp.ResponseData.Trains)...)
	result.Lines = append(result.Lines, parseLines(realtimeResp.ResponseData.Buses)...)
	result.Lines = append(result.Lines, parseLines(realtimeResp.ResponseData.Trams)...)
	result.Lines = append(result.Lines, parseLines(realtimeResp.ResponseData.Ships)...)

	return c.JSON(http.StatusOK, result)
}

func toDisplayName(rawName string) string {
	res := strings.ReplaceAll(rawName, "(Stockholm)", "")
	res = strings.ReplaceAll(res, "(Lidingö)", "")
	res = strings.ReplaceAll(res, "(Nacka)", "")
	res = strings.ReplaceAll(res, "(Salem)", "")
	res = strings.TrimSpace(res)
	return res
}

func parseSLTime(raw string) (time.Time, error) {
	const format = "2006-01-02T15:04:05"
	loc, err := time.LoadLocation("Europe/Stockholm")
	if err != nil {
		return time.Time{}, err
	}
	return time.ParseInLocation(format, raw, loc)
}

func parseLines(raw []realtimeLine) []siteLine {
	lineMap := make(map[string]siteLine, len(raw))
	for _, rawLine := range raw {
		line := siteLine{
			Type:        parseLineType(rawLine),
			Number:      rawLine.LineNumber,
			Destination: rawLine.Destination,
			Direction:   rawLine.JourneyDirection,
			Departures:  []departure{},
		}
		key := createHashKeyForLine(line)
		if existingLine, found := lineMap[key]; found {
			line = existingLine
		}
		dep, err := parseLineDeparture(rawLine)
		if err != nil {
			logger.Warnf("failed to parse line, skipping: %w", err)
			continue
		}
		line.Departures = append(line.Departures, dep)
		lineMap[key] = line
	}
	result := []siteLine{}
	for _, line := range lineMap {
		result = append(result, line)
	}
	// TODO: Perhaps sort
	return result
}

func parseLineType(raw realtimeLine) string {
	transportMode := strings.ToLower(strings.TrimSpace(raw.TransportMode))
	switch transportMode {
	case "metro":
		groupOfLine := strings.ToLower(strings.TrimSpace(raw.GroupOfLine))
		if strings.Contains(groupOfLine, "grön") {
			return typeMetroGreen
		} else if strings.Contains(groupOfLine, "röd") {
			return typeMetroRed
		} else if strings.Contains(groupOfLine, "blå") {
			return typeMetroBlue
		} else {
			logger.Warnf("unknown metro line: %s", groupOfLine)
			return typeUnk
		}
	case "bus":
		return typeBus
	case "train":
		return typeTrain
	case "tram":
		return typeTram
	case "ship":
		return typeShip
	default:
		logger.Warnf("unknown transport mode: %s", transportMode)
		return typeUnk
	}
}

func parseLineDeparture(raw realtimeLine) (departure, error) {
	deviations := []deviation{}
	for _, dev := range raw.Deviations {
		deviations = append(deviations, parseLineDeviation(dev))
	}
	plannedTime, err := time.ParseInLocation(slTimeFormat, raw.TimeTabledDateTime, slTimeLocation)
	if err != nil {
		return departure{}, fmt.Errorf("failed to parse planned departure time: %w", err)
	}
	result := departure{
		Deviations:  deviations,
		Planned:     plannedTime,
		Expected:    time.Time{},
		HasExpected: false,
	}
	if raw.ExpectedDateTime != "" {
		expectedTime, err := time.ParseInLocation(slTimeFormat, raw.ExpectedDateTime, slTimeLocation)
		if err != nil {
			logger.Debugf("Failed to parse expected time, ignoring...: %w", err)
		} else {
			result.Expected = expectedTime
			result.HasExpected = true
		}
	}
	return result, nil
}

func parseLineDeviation(raw realtimeDeviation) deviation {
	return deviation{}
}

func createHashKeyForLine(line siteLine) string {
	var buf bytes.Buffer
	buf.WriteString(line.Type)
	buf.WriteString("|")
	buf.WriteString(line.Number)
	buf.WriteString("|")
	buf.WriteString(line.Destination)
	buf.WriteString("(")
	buf.WriteString(strconv.Itoa(line.Direction))
	buf.WriteString(")")
	return buf.String()
}

func init() {
	lookupKey, found := os.LookupEnv("SL_KEY_LOOKUP")
	if !found {
		log.Fatal("No lookup key")
	}
	slLookupKey = lookupKey
	realtimeKey, found := os.LookupEnv("SL_KEY_REALTIME")
	if !found {
		log.Fatal("No realtime key")
	}
	slRealtimeKey = realtimeKey

	loc, err := time.LoadLocation("Europe/Stockholm")
	if err != nil {
		log.Fatal("Failed to load timezone data")
	}
	slTimeLocation = loc
	if p, found := os.LookupEnv("PORT"); found {
		portToUse = p
	}
}

func main() {
	e := echo.New()
	e.Debug = true
	e.HideBanner = true
	logger = e.Logger
	e.Use(middleware.Logger())
	e.Use(middleware.CORS())
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello world!")
	})
	e.GET("/search", searchSite)
	e.GET("/departures/:id", getDeparturesForSite)
	logger.Fatal(e.Start(":" + portToUse))
}
